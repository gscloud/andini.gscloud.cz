<?php
/**
 * GSC Tesseract
 * php version 8.2
 *
 * @category CMS
 * @package  Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://github.com/GSCloud/lasagna
 */

namespace GSC;

use Cake\Cache\Cache;
use GSC\StringFilters as SF;

/**
 * Andini Presenter class
 * 
 * @category CMS
 * @package  Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://github.com/GSCloud/lasagna
 */
class AndiniPresenter extends APresenter
{
    // short codes processor flags
    const PROCESSOR_FLAGS = SF::GALLERY_RANDOM | SF::LAZY_LOADING;

    /**
     * Main controller
     *
     * @param mixed $param optional parameter
     * 
     * @return object Controller
     */
    public function process($param = null)
    {
        // rate limiting
        $this->checkRateLimit();

        // get Presenter
        $presenter = $this->getPresenter();
        if (!\is_array($presenter)) {
            return $this;
        }

        // get View
        $view = $this->getView();
        if (!$view) {
            return $this;
        }

        // get Model
        $data = $this->getData();

        // HTML header + expand Model
        $this->setHeaderHtml()->dataExpander($data);

        // menu variable for content switching
        $data[$view . '_menu'] = true;

        switch ($view) {
        case "cedulky":
        case "cedulky2":
            // force admin log-in
            $g = $this->getUserGroup();
            if ($g != 'admin') {
                if (ob_get_level()) {
                    ob_end_clean();
                }
                $this->setLocation('https://andini.gscloud.cz/login');
                exit;
            }
            break;
            
        default:
            break;
        }

        // process shortcodes, fix HTML and translations
        $lang = $data['lang'] ?? 'en';
        foreach ($data['l'] ??=[] as $k => $v) {
            if (\str_starts_with($v, '[markdown]')) {
                SF::shortCodesProcessor($data['l'][$k], self::PROCESSOR_FLAGS);
            } elseif (\str_starts_with($v, '[markdownextra]')) {
                SF::shortCodesProcessor($data['l'][$k], self::PROCESSOR_FLAGS);
            } else {
                SF::convertEolToBrNbsp($data['l'][$k]);
            }
            SF::correctTextSpacing($data['l'][$k], $lang);
        }

        // render
        $output = '';
        if ($data) {
            $output = $this->setData(
                $data
            )->renderHTML(
                $presenter[$view]["template"]
            );
        }

        SF::trimHtmlComment($output);
        return $this->setData("output", $output);
    }
}
