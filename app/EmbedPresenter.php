<?php
/**
 * GSC Tesseract
 *
 * @category Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 */

namespace GSC;

/**
 * Home Presenter
 */
class EmbedPresenter extends APresenter
{
    /** @var array names of days */
    const DAYS = [
        "cs" => [
            1 => "Pondělí",
            2 => "Úterý",
            3 => "Středa",
            4 => "Čtvrtek",
            5 => "Pátek",
        ],
        "en" => [
            1 => "Monday",
            2 => "Tuesday",
            3 => "Wednesday",
            4 => "Thursday",
            5 => "Friday",
        ],
    ];

    /**
     * Get food type string
     *
     * @return array CS + EN strings
     */
    private function getTypeString($type)
    {
        switch ((int) $type) {
            case 1:
            case 2:
            case 3:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 25:
            case 67:
                $cs = "Snídaně a snack";
                $en = "Breakfast and snacks";
                break;
            case 24:
                $cs = "Pečivo";
                $en = "Pastries";
                break;
            case 26:
            case 30:
            case 31:
            case 33:
            case 38:
            case 39:
                $cs = "Saláty";
                $en = "Salads";
                break;
            case 40:
            case 45:
                $cs = "Nápoje";
                $en = "Drinks";
                break;
            case 41:
                $cs = "Káva";
                $en = "Coffee";
                break;
            case 42:
                $cs = "Nealkoholické nápoje";
                $en = "Non-alcoholic Drinks";
                break;
            case 43:
            case 44:
                $cs = "Alkoholické nápoje";
                $en = "Alcoholic Drinks";
                break;
            case 46:
                $cs = "Teplé nápoje";
                $en = "Hot Drinks";
                break;
            case 47:
                $cs = "Koktejly";
                $en = "Cocktails";
                break;
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
                $cs = "Tapas";
                $en = "Tapas";
                break;
            case 61:
            case 62:
            case 63:
            case 71:
            case 72:
            case 73:
            case 74:
            case 81:
            case 82:
                $cs = "Dorty, dezerty a koláče";
                $en = "Cakes, Desserts and Pies";
                break;
            case 78:
                $cs = "Zmrzliny";
                $en = "Ice Creams";
                break;
            default:
                $cs = "ostatní";
                $en = "other";
                break;
        }
        // export
        return [
            "cs" => $cs,
            "en" => $en,
        ];
    }

    /**
     * Main controller
     *
     * @return object Singleton instance
     */
    public function process()
    {
        // basic setup
        $data = $this->getData();
        $presenter = $this->getPresenter();
        $view = $this->getView();
        $match = $this->getMatch();
        $module = $match["params"]["module"] ?? null;
        $this->checkRateLimit()->setHeaderHtml()->dataExpander($data); // Model
        switch ($module) {
            case "rozvoz":
            case "rozvoz1":
            case "rozvoz2":
                $api = json_decode(\file_get_contents($data["base"] . "api/v1/GetLunchMenu"), true);
                break;
            case "rozvozx":
            case "rozvoz1x":
            case "rozvoz2x":
                $api = json_decode(\file_get_contents($data["base"] . "api/v1/GetLunchMenuT2"), true);
                break;
            case "menu":
            case "menu1":
            case "menu2":
                $api = json_decode(\file_get_contents($data["base"] . "api/v1/GetAcademiaMenu"), true);
                break;
            case "menux":
            case "menu1x":
            case "menu2x":
                $api = json_decode(\file_get_contents($data["base"] . "api/v1/GetAcademiaMenuT2"), true);
                break;
        }

        $t2 = null; // false = current week, true = next week
        $lang = $data["lang"];

        // process VIEW
        switch ($view) {
            case "cs_embed":
            case "cs_print":
                $view = "cs_embed";
                break;
            case "en_embed":
            case "en_print":
                $view = "en_embed";
                break;
        }

        switch ($module) {
            case "menu1":
                $t2 = false;
            case "menu1x":
                if (is_null($t2)) {
                    $t2 = true;
                }
                if ($lang == "en") {
                    $x = $data["l"]["week_menu_info_ac"] ?? "";
                    StringFilters::convertEolToBr($x);
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_act2"] ?? "") . "</h4>\n" . $x;
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_ac"] ?? "") . "</h4>\n" . $x;
                    }
                } else {
                    $x = $data["l"]["week_menu_info_ac"] ?? "";
                    StringFilters::convertEolToBr($x);
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_act2"] ?? "") . "</h4>\n" . $x;
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_ac"] ?? "") . "</h4>\n" . $x;
                    }
                }
                if ($api && $api["data"] && count($api["data"])) {
                    $s = "";
                    $q = $api["data"];
                    if ($lang == "cs") {
                        for ($d = 1; $d < 6; $d++) {
                            $h = "<h5>🌞&nbsp;" . self::DAYS[$lang][$d] . "</h5>";
                            $a = $this->searchmenu($q, $d, "A");
                            $b = $this->searchmenu($q, $d, "B");
                            if (strlen($a . $b)) {
                                $s .= $h . "$a<br>$b";
                            } else {
                                //$s .= $data["l"]["week_menu_empty"] ?? "&mdash; &mdash; &mdash; ";
                            }
                        }
                    } else {
                        for ($d = 1; $d < 6; $d++) {
                            $h = "<h5>🌞&nbsp;" . self::DAYS[$lang][$d] . "</h5>";
                            $a = $this->searchmenu2($q, $d, "A");
                            $b = $this->searchmenu2($q, $d, "B");
                            if (strlen($a . $b)) {
                                $s .= $h . "$a<br>$b";
                            } else {
                                //$s .= $data["l"]["week_menu_empty"] ?? "&mdash; &mdash; &mdash; ";
                            }
                        }
                    }
                    $data["embed"] .= $s;
                }
                break;

            case "rozvoz1":
                $t2 = false;
            case "rozvoz1x":
                if (is_null($t2)) {
                    $t2 = true;
                }
                if ($lang == "en") {
                    $x = $data["l"]["week_menu_info"] ?? "";
                    StringFilters::convertEolToBr($x);
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_t2"] ?? "") . "</h4>\n" . $x;
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date"] ?? "") . "</h4>\n" . $x;
                    }
                } else {
                    $x = $data["l"]["week_menu_info"] ?? "";
                    StringFilters::convertEolToBr($x);
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_t2"] ?? "") . "</h4>\n" . $x;
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date"] ?? "") . "</h4>\n" . $x;
                    }
                }
                if ($api && $api["data"] && count($api["data"])) {
                    $s = "";
                    $q = $api["data"];
                    if ($lang == "cs") {
                        for ($d = 1; $d < 6; $d++) {
                            $h = "<h5>🌞&nbsp;" . self::DAYS[$lang][$d] . "</h5>";
                            $a = $this->searchmenu($q, $d, "A");
                            $b = $this->searchmenu($q, $d, "B");
                            if (strlen($a . $b)) {
                                $s .= $h . "$a<br>$b";
                            } else {
                                //$s .= $data["l"]["week_menu_empty"] ?? "&mdash; &mdash; &mdash; ";
                            }
                        }
                    } else {
                        for ($d = 1; $d < 6; $d++) {
                            $h = "<h5>🌞&nbsp;" . self::DAYS[$lang][$d] . "</h5>";
                            $a = $this->searchmenu2($q, $d, "A");
                            $b = $this->searchmenu2($q, $d, "B");
                            if (strlen($a . $b)) {
                                $s .= $h . "$a<br>$b";
                            } else {
                                //$s .= $data["l"]["week_menu_empty"] ?? "&mdash; &mdash; &mdash; ";
                            }
                        }
                    }
                    $data["embed"] .= $s;
                }
                break;

            case "menu2":
                $t2 = false;
            case "menu2x":
                if (is_null($t2)) {
                    $t2 = true;
                }
                if ($lang == "en") {
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_act2"] ?? "") . "</h4>\n";
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_ac"] ?? "") . "</h4>\n";
                    }
                } else {
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_act2"] ?? "") . "</h4>\n";
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_ac"] ?? "") . "</h4>\n";
                    }
                }
                if ($api && $api["data"] && count($api["data"])) {
                    $s = "";
                    $q = $api["data"];
                    if ($lang == "cs") {
                        $s .= $this->searchmenu($q);
                        $s .= "<br>";
                    } else {
                        $s .= $this->searchmenu2($q);
                        $s .= "<br>";
                    }
                    $data["embed"] .= $s;
                }
                break;

            case "rozvoz2":
                $t2 = false;
            case "rozvoz2x":
                if (is_null($t2)) {
                    $t2 = true;
                }
                if ($lang == "en") {
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_t2"] ?? "") . "</h4>\n";
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date"] ?? "") . "</h4>\n";
                    }
                } else {
                    if ($t2) {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date_t2"] ?? "") . "</h4>\n";
                    } else {
                        $data["embed"] = "<h4>" . ($data["l"]["week_menu_date"] ?? "") . "</h4>\n";
                    }
                }
                if ($api && $api["data"] && count($api["data"])) {
                    $s = "";
                    $q = $api["data"];
                    if ($lang == "cs") {
                        $s .= $this->searchmenu($q);
                        $s .= "<br>";
                    } else {
                        $s .= $this->searchmenu2($q);
                        $s .= "<br>";
                    }
                    $data["embed"] .= $s;
                }
                break;

            default:
                // error
                return ErrorPresenter::getInstance()->process(404);
        }
        // output rendering
        $output = $this->setData($data)->renderHTML($presenter[$view]["template"]);
        return $this->setData("output", $output); // save model
    }

    // search the menu in CS
    private function searchmenu($q, $day = null, $menu = null)
    {
        $lt = null;
        $out = [];
        $filter = trim(strtolower($_GET["filter"] ?? ""));
        foreach ($q as $x) {
            if ($filter == "star") {
                if (!$x["star"]) {
                    continue;
                }
            }
            if ($day != $x["day"]) {
                continue;
            }
            if ($menu != $x["menu"]) {
                continue;
            }
            if (\is_null($day) && \is_null($menu)) {
                $zs = $this->getTypeString($x["type"])["cs"];
                if ($lt != $zs) {
                    $lt = $zs;
                    $out[] = "<br><b>" . $zs . "</b>\n";
                }
            }
            $alg = str_replace(" ", "", $x["allergens"] ?? "");
            $flag = str_replace(" ", "&nbsp;", $x["flags"] ?? "");
            $price = $x["price"] ?? "";
            $title = $x["title"] ?? "";
            $vol = str_replace(" ", "&nbsp;", $x["volume"] ?? "");
            if (strlen($alg)) {
                $alg = "($alg)";
            }
            $out[] = "<span class=volume>$vol</span>&nbsp; <span class=title style='font-size:105%'><b>$title</b></span> <span class=flags>$flag</span> <span class=allergens><i>$alg</i></span> — <span class=price>$price&nbsp;Kč</span>\n";
        }
        return implode("<br>", $out);
    }

    // search the menu in EN
    private function searchmenu2($q, $day = null, $menu = null)
    {
        $lt = null;
        $out = [];
        $filter = trim(strtolower($_GET["filter"] ?? ""));
        foreach ($q as $x) {
            if ($filter == "star") {
                if (!$x["star"]) {
                    continue;
                }
            }
            if ($day != $x["day"]) {
                continue;
            }
            if ($menu != $x["menu"]) {
                continue;
            }
            if (\is_null($day) && \is_null($menu)) {
                $zs = $this->getTypeString($x["type"])["en"];
                if ($lt != $zs) {
                    $lt = $zs;
                    $out[] = "<br><b>" . $zs . "</b>\n";
                }
            }
            $alg = str_replace(" ", "", $x["allergens_en"] ?? "");
            $flag = str_replace(" ", "&nbsp;", $x["flags_en"] ?? "");
            $price = $x["price"] ?? "";
            $title = $x["title_en"] ?? "";
            $vol = str_replace(" ", "&nbsp;", $x["volume_en"] ?? "");
            if (strlen($alg)) {
                $alg = "($alg)";
            }
            $out[] = "<span class=volume>$vol</span>&nbsp; <span class=title style='font-size:105%'><b>$title</b></span> <span class=flags>$flag</span> <span class=allergens><i>$alg</i></span> — <span class=price>CZK&nbsp;$price</span>\n";
        }
        return implode("<br>", $out);
    }

}
