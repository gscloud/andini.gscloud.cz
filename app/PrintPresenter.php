<?php
/**
 * GSC Tesseract
 *
 * @category Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 */

namespace GSC;

/**
 * Home Presenter
 */
class PrintPresenter extends APresenter
{
    /**
     * Main controller
     *
     * @return object Singleton instance
     */
    public function process()
    {
        // basic setup
        $data = $this->getData();
        $presenter = $this->getPresenter();
        $view = $this->getView();
        $match = $this->getMatch();
        $module = $match["params"]["module"] ?? null;
        $this->checkRateLimit()->setHeaderHtml()->dataExpander($data); // expand the Model

        $lang = $data["lang"];
        $filter = trim(strtolower($_GET["filter"] ?? ""));
        $param = "";
        if ($filter) {
            $param = "?filter=$filter";
        }

        // process VIEW
        switch ($view) {
            case "cs_print":
            case "en_print":
                switch ($module) {
                    case "menu":
                        $p1 = @\file_get_contents($data["base"] . "$lang/embed/menu1$param?nonce=" . time()) ?? null;
                        $p2 = @\file_get_contents($data["base"] . "$lang/embed/menu2$param?nonce=" . time()) ?? null;
                        if (\is_null($p1) || \is_null($p2)) {
                            return ErrorPresenter::getInstance()->process(500);
                        }
                        $data["print1"] = $p1;
                        $data["print2"] = $p2;
                        $data["no-footer"] = true;
                        $output = $this->setData($data)->renderHTML("print2p"); // 2p template
                        return $this->setData("output", $output); // save model
                        break;

                    case "menux":
                        $p1 = @\file_get_contents($data["base"] . "$lang/embed/menu1x$param?nonce=" . time()) ?? null;
                        $p2 = @\file_get_contents($data["base"] . "$lang/embed/menu2x$param?nonce=" . time()) ?? null;
                        if (\is_null($p1) || \is_null($p2)) {
                            return ErrorPresenter::getInstance()->process(500);
                        }
                        $data["print1"] = $p1;
                        $data["print2"] = $p2;
                        $data["no-footer"] = true;
                        $output = $this->setData($data)->renderHTML("print2p"); // 2p template
                        return $this->setData("output", $output); // save model
                        break;

                    case "menu1":
                    case "menu1x":
                    case "menu2":
                    case "menu2x":
                        $data["print"] = EmbedPresenter::getInstance()->setData($data)->process()->getData()["output"];
                        $data["no-footer"] = true;
                        break;

                    case "rozvoz":
                        $p1 = @\file_get_contents($data["base"] . "$lang/embed/rozvoz1$param?nonce=" . time()) ?? null;
                        $p2 = @\file_get_contents($data["base"] . "$lang/embed/rozvoz2$param?nonce=" . time()) ?? null;
                        if (\is_null($p1) || \is_null($p2)) {
                            return ErrorPresenter::getInstance()->process(500);
                        }
                        $data["print1"] = $p1;
                        $data["print2"] = $p2;
                        $output = $this->setData($data)->renderHTML("print2p"); // 2p template
                        return $this->setData("output", $output); // save model
                        break;

                    case "rozvozx":
                        $p1 = @\file_get_contents($data["base"] . "$lang/embed/rozvoz1x$param?nonce=" . time()) ?? null;
                        $p2 = @\file_get_contents($data["base"] . "$lang/embed/rozvoz2x$param?nonce=" . time()) ?? null;
                        if (\is_null($p1) || \is_null($p2)) {
                            return ErrorPresenter::getInstance()->process(500);
                        }
                        $data["print1"] = $p1;
                        $data["print2"] = $p2;
                        $output = $this->setData($data)->renderHTML("print2p"); // 2p template
                        return $this->setData("output", $output); // save model
                        break;

                    case "rozvoz1":
                    case "rozvoz1x":
                    case "rozvoz2":
                    case "rozvoz2x":
                        $data["print"] = EmbedPresenter::getInstance()->setData($data)->process()->getData()["output"];
                        break;
                }
                $output = $this->setData($data)->renderHTML($presenter[$view]["template"]);
                return $this->setData("output", $output); // save model
                break;
        }
        return ErrorPresenter::getInstance()->process(404);
    }
}
