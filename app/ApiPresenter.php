<?php
/**
 * GSC Tesseract
 * php version 8.2
 *
 * @category CMS
 * @package  Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://github.com/GSCloud/lasagna
 */

namespace GSC;

use Cake\Cache\Cache;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use Nette\Neon\Neon;
use RedisClient\RedisClient;
use GSC\StringFilters as SF;

/**
 * API Presenter
 *
 * @category CMS
 * @package  Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://github.com/GSCloud/lasagna
 */
class ApiPresenter extends APresenter
{
    const API_CACHE = LOCALHOST ? "tenseconds" : "minute";
    const API_CACHE2 = LOCALHOST ? "tenseconds" : "fiveminutes";
    const ACCESS_TIME_LIMIT = 3599;
    const MAX_API_HITS = 1000;
    const USE_CACHE = true;

    // price tags CSV header
    const PRICETAGS_HEADER = "STAR,DIS,TITLE,TITLE_EN,"
        . "PROD,PRICE,ALG,TAG,IMAGE,CAT,VENUE";

    // traffic CSV header
    const TRAFFIC_HEADER = "ADDRESS,VALUE,TIME";
    const TRAFFIC_REMOTE = '2PACX-1vSHDNZEeJ08FBQAqyHHSvxfvP'
        . '__wZXbJ6l6NFOMpY1IRSbD_TS6eA-1umfb8GpJZJhk_m-CwZBJloj9';
    const TRAFFIC_STREETS = APP . '/traffic_streets.neon';

    /**
     * Main controller
     * 
     * @param mixed $param optional parameter
     * 
     * @return object Controller
     */
    public function process($param = null)
    {
        \setlocale(LC_ALL, "cs_CZ.utf8");
        \error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

        $data = $this->getData();
        if (!\is_array($data)) {
            return $this;
        }
        $cfg = $this->getCfg();
        if (!\is_array($cfg)) {
            return $this;
        }
        $match = $this->getMatch();
        $view = $this->getView();
        if (!\is_string($view)) {
            return $this;
        }

        // API usage
        $api_usage = $this->accessLimiter();
        if (!\is_int($api_usage)) {
            $api_usage = 0;
        }

        // general API properties
        $extras = [
            "name" => "ANDINI REST API",
            "fn" => $view,
            "endpoint" => \explode("?", $_SERVER['REQUEST_URI'])[0],
            "api_usage" => $api_usage,
            "api_quota" => (int) self::MAX_API_HITS,
            "access_time_limit" => self::ACCESS_TIME_LIMIT,
            "cached" => self::USE_CACHE,
            "cache_time_limit" => $data["cache_profiles"][self::API_CACHE],
            "uuid" => $this->getUID(),
        ];

        // API calls
        switch ($view) {
        case "GetPriceTags":
            $g = $this->getUserGroup();
            if ($g != "admin") {
                if (\ob_get_level()) {
                    \ob_end_clean();
                }
                \header("Location: /login", true, 303);
                exit;
            }
            $this->checkPermission("admin");

            if (self::USE_CACHE) {
                $data = Cache::read($view, self::API_CACHE);
                if (\is_array($data)) {
                    return $this->writeJsonData($data, $extras);
                }
            }

            $this->preloadAppData();
            $data = $this->readPriceTags();
            if (\is_array($data)) {
                Cache::write($view, $data, self::API_CACHE);
                return $this->writeJsonData($data, $extras);
            }
            return ErrorPresenter::getInstance()->process(404);

        case "GetTraffic":
            $extras["cache_time_limit"]
                = $data["cache_profiles"][self::API_CACHE2];
            if (self::USE_CACHE) {
                $data = Cache::read($view, self::API_CACHE2);
                if (\is_array($data)) {
                    return $this->writeJsonData($data, $extras);
                }
            }
            $data = $this->readTraffic();
            if (\is_array($data)) {
                Cache::write($view, $data, self::API_CACHE2);
                return $this->writeJsonData($data, $extras);
            }
            return ErrorPresenter::getInstance()->process(404);

        default:
            return ErrorPresenter::getInstance()->process();
        }
    }

    /**
     * Read price tags
     *
     * @return array|mixed price tag data
     */
    public function readPriceTags()
    {
        \setlocale(LC_ALL, 'cs_CZ.utf8');

        if (!\is_string($csv = $this->readAppData("pricetags"))) {
            return [];
        }

        // phpstan
        $STAR = ['x'];$DIS = ['x'];$TITLE = ['x'];$TITLE_EN = ['x'];
        $PROD = ['x'];$PRICE = ['x'];$ALG = ['x'];$TAG = ['x'];
        $IMAGE = ['x'];$CAT = ['x'];$VENUE = ['x'];

        $columns = \explode(',', self::PRICETAGS_HEADER);
        try {
            foreach ($columns as $col) {
                $$col = [];
                $reader = Reader::createFromString($csv);
                $reader->setHeaderOffset(0);
                $records = (new Statement())->offset(1)->process($reader);
                $i = 0;
                foreach ($records->fetchColumn($col) as $x) {
                    $$col[$i] = $x;
                    $i++;
                }
            }
        } catch (\Exception $e) {
            return [];
        }
        
        $arr = [];
        $base58 = new \Tuupola\Base58;
        for ($i = 0; $i < \count($TITLE); $i++) {
            // skip DIS rows
            if (\strlen(\trim($DIS[$i]))) {
                continue;
            }

            // skip no TITLE rows
            if (!\strlen(\trim($TITLE[$i]))) {
                continue;
            }

            // skip no STAR rows
            if (!\strlen(\trim($STAR[$i]))) {
                continue;
            }

            // parse PRICE
            $x = $PRICE[$i];
            $p = [];
            if (\is_string($x)) {
                \preg_match('!(\d+)!', $x, $p);
            }
            $PRICE[$i] = $p[1] ?? 0;

            // skip incorrect PRICE rows
            if (!$PRICE[$i]) {
                continue;
            }

            // fix TITLE, TITLE_EN spacing
            SF::correctTextSpacing($TITLE[$i], "cs");
            SF::correctTextSpacing($TITLE_EN[$i], "en");

            // parse TAG
            $x = $TAG[$i];
            if (!\is_string($x)) {
                $x = '';
            }
            $x = \explode(" ", \strtoupper(\strtr(\trim($x), ",;:-", "    ")));
            // @phpstan-ignore-next-line
            $x = \array_filter($x, 'trim');
            $x = \array_filter(
                $x, function ($x) {
                    return \strlen($x) > 0;
                }
            );
            $x = \array_unique($x);
            \sort($x);
            $x = $TAG_ORIG[$i] = \implode(" ", $x);

            // DF = dairy-free
            $s = 40;
            $x = \str_replace(
                "DF",
                '<img src="/img/dairy-free.svg" width=' . $s .' height=' . $s .'> ',
                $x
            );
            // GF = gluten-free
            $x = \str_replace(
                "GF",
                '<img src="/img/gluten-free.svg" width=' . $s .' height=' . $s .'> ',
                $x
            );
            // MA = maso
            $x = \str_replace("MA", "", $x);
            // ST = sladká tečka
            $x = \str_replace("ST", "", $x);
            // VE = vegetarian
            $x = \str_replace("VE", "🍀 ", $x);
            // VG = vegan
            $x = \str_replace(
                "VG",
                '<img src="/img/vegan.svg" width=' . $s .' height=' . $s .'> ',
                $x
            );
            $x = \preg_replace('!\s+!', ' ', $x);
            if (\is_string($x)) {
                $x = \trim(\trim($x), ",");
            }
            $TAG[$i] = $x;

            // parse ALG
            if (\is_string($ALG[$i])) {
                $ALG[$i] = \preg_replace('!\s+!', ' ', $ALG[$i]);
            }

            // unique id
            $id = \substr(
                $base58->encode(
                    \hash(
                        "sha256",
                        $TITLE[$i]
                        . $PRICE[$i]
                        . $PROD[$i]
                        . $VENUE[$i]
                    )
                ), 0, 32
            );

            // skip duplicate records
            if (\array_key_exists($id, $arr)) {
                continue;
            }

            // export
            $arr[] = [
                "id" => $id,
                "title" => $TITLE[$i],
                "title_en" => $TITLE_EN[$i],
                "price" => (int) $PRICE[$i],
                "tags" => $TAG[$i],
                "tags_text" => $TAG_ORIG[$i],
                "allergens" => $ALG[$i],
                "producer" => $PROD[$i],
                "category" => $CAT[$i],
                "venue" => $VENUE[$i],
            ];
        }
        return $arr;
    }

    /**
     * Read traffic
     * 
     * @return array|mixed traffic data
     */
    public function readTraffic()
    {
        // compile streets
        $streets_list = @\file_get_contents(self::TRAFFIC_STREETS);
        if (!$streets_list) {
            return null;
        }
        $streets = (array) @Neon::decode($streets_list);
        if (!\count($streets)) {
            return null;
        }

        // fetch remote traffic data
        $remote = self::GS_CSV_PREFIX
            . self::TRAFFIC_REMOTE
            . self::GS_CSV_POSTFIX;
        try {
            $csv = @\file_get_contents($remote);
        } catch (\Exception $e) {
            return [];
        }
        if (!\is_string($csv)) {
            return [];
        }

        // phpstan
        $ADDRESS = ['x'];
        $VALUE = ['x'];

        $columns = \explode(',', self::TRAFFIC_HEADER);
        try {
            foreach ($columns as $col) {
                $$col = [];
                $reader = Reader::createFromString($csv);
                $reader->setHeaderOffset(0);
                $records = (new Statement())->offset(1)->process($reader);
                $i = 0;
                foreach ($records->fetchColumn($col) as $x) {
                    $$col[$i] = $x;
                    $i++;
                }
            }
        } catch (\Exception $e) {
            return null;
        }

        $average_traffic = 0;
        for ($i = 0; $i < \count($ADDRESS); $i++) {
            if (\in_array($ADDRESS[$i], $streets)) {
                $average_traffic += (int) $VALUE[$i];
            }
        }
        return [
            "average_traffic" => (float)
                \round((float) ($average_traffic / count($streets)), 3),
            //"streets" => $streets,
        ];
    }

    /**
     * Reformat date string
     *
     * @param string $x string to reformat
     * 
     * @return string string in correct format
     */
    function reformatDate($x)
    {
        if (!$x) {
            return "";
        }
        $x = \explode('.', $x);
        if (\count($x) == 3) {
            if (\strlen($x[2]) == 4) {
                if (\strlen($x[0]) < 2) {
                    $x[0] = "0{$x[0]}";
                }
                if (\strlen($x[1]) < 2) {
                    $x[1] = "0{$x[1]}";
                }
                return "{$x[0]}.{$x[1]}.{$x[2]}";
            }
        }
        return "";
    }

    /**
     * Redis access limiter
     *
     * @return mixed access count or 0
     */
    public function accessLimiter()
    {
        $hour = \date('H');
        $uid = $this->getUID();
        $key = 'access_limiter_' . SERVER . '_' . PROJECT . "_{$hour}_{$uid}";
        \error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
        $host = $this->getData('redis.host');
        if (!\is_string($host)) {
            $host = 'localhost';
        }
        $port = $this->getData('redis.port');
        if (!\is_string($port) || !\is_numeric($port)) {
            $port = 6377;
        }
        $redis = new RedisClient(
            [
            'server' => "$host:$port",
            'timeout' => 1,
            ]
        );
        try {
            $val = (int) @$redis->get($key);
        } catch (\Exception $e) {
            return 0;
        }
        if ($val > self::MAX_API_HITS) {
            $this->setLocation('/err/420');
        }
        try {
            @$redis->multi();
            @$redis->incr($key);
            @$redis->expire($key, self::ACCESS_TIME_LIMIT);
            @$redis->exec();
        } catch (\Exception $e) {
            return 0;
        }
        $val++;
        return $val;
    }
}
